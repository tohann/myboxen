class people::tohann {
  include java
  include vagrant
  include virtualbox
  include sequel_pro
  include firefox
  include github_for_mac
  include sublime_text_2
  include wget
  include libtool
  include pkgconfig
  include pcre
  include libpng
  include autoconf
  include mysql
  include jmeter
  include iterm2::stable
  include jumpcut
  include autojump
  include atom


  $home     = "/Users/${::boxen_user}"

}
